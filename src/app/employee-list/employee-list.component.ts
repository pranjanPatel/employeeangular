import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Employee } from '../employee';
import { EmployeeService } from '../employee.service';
import { FormGroup, NgForm } from '@angular/forms';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {

  employee: Employee = new Employee();
  employees: Observable<Employee[]>;
  addEmployeeForm: FormGroup;
  isExist: boolean = false;
  successmsg: string;
  isShowModal: boolean = false;
  role = [
    {id: 1, name: "tester"},
    {id: 2, name: "developer"},
    {id: 3, name: "admin"},
    {id: 4, name: "manager"}
  ];

  constructor(private employeeService: EmployeeService) { }

  ngOnInit() {
    this.reloadData();
  }

  newEmployee(): void { this.employee = new Employee();
  }
  reloadData() {
    this.employees = this.employeeService.getEmployeesList();
  }

  save(myForm: NgForm): void {
    this.isExist = false;
    this.employeeService.checkEmployeeByEmpId(this.employee.employeeId, this.employee.id)
      .subscribe(data =>  {
        if (data == "false"){
          this.employeeService.createEmployee(this.employee)
          .subscribe(data =>  this.reloadData(), error => console.log(error));
          this.employee = new Employee();
          myForm.resetForm();
          this.successmsg = 'Employee saved successfully...!';
          this.displaySavedMsg();
        } else {
          this.isExist = true;
        }
      } );
  }

  onSubmit(myForm: NgForm): void {
      this.save(myForm);
  }
  deleteEmployee(id: number) {
    this.employeeService.deleteEmployee(id)
      .subscribe(
        data => {
          console.log(data);
          this.reloadData();
        },
        error => console.log(error));
  }
  editEmployee(id: number) {
    this.employeeService.getEmployee(id)
      .subscribe(
        data => {
         this.employee = data;
        },
        error => console.log(error));
  }
  displaySavedMsg(){
    this.isShowModal = true;
    setTimeout(()=>{
      this.isShowModal = false;
    }, 3000);
  }
  deleteConfirm(name: string , id : number) {
    if(confirm('Are you sure to delete ' + name)) {
      this.deleteEmployee(id);
    }
  }
}
