export class Employee {
    id: number;
    firstName: string;
    employeeId: string;
    lastName: string;
    gender: boolean;
    role: number;
    pincode: number;
    email: string;
}
